%web_drop_table(WORK.IMPORT);
FILENAME REFFILE '/folders/myfolders/rel_trebeschi.csv';
PROC IMPORT DATAFILE=REFFILE
	DBMS=CSV
	OUT=WORK.IMPORT;
	GETNAMES=YES;
    guessingrows=600000; /* if omitted, the default is 20 */

RUN;

PROC CONTENTS DATA=WORK.IMPORT; RUN;
%web_open_table(WORK.IMPORT);
proc freq data = work.import;
table pointname;
run;

data import2;
set  import;
if index(pointname,'tensao_21A,2016/07/24 10:18:42') > 0 then do;
pointname = 'TREBESCHI-NS200_H-R4FA125 - tensao_21A';
time = '24JUL2016:10:18:42'dt;
value = 6518.0;
end;

if index(pointname,'tensao_18,2016/07/24 18:11:00') > 0 then do;
pointname = 'TREBESCHI-NS200_H-R4FA128 - tensao_18';
time = '24JUL2016:18:11:00'dt;
value = 6524.0;
end;
run;

proc freq data = work.import2;
table pointname;
run;
data teste;
set import2;
where pointname eq 'TREBESCHI-NS126-R4FA130 - radiacao_acumulada_19A';
run;

/*
-REBESCHI-NS126-R4FA130 - DPV	                    96891	15.59	96891	15.59
-TREBESCHI-NS126-R4FA130 - Temperatura	            96699	15.56	193590	31.15
-TREBESCHI-NS126-R4FA130 - Umidade	                96891	15.59	290481	46.75
TREBESCHI-NS126-R4FA130 - radiacao_acumulada_18	    45491	7.32	335972	54.07
TREBESCHI-NS126-R4FA130 - radiacao_acumulada_19A	18652	3.00	354624	57.07
TREBESCHI-NS126-R4FA130 - radiacao_acumulada_19B	18613	3.00	373237	60.06
TREBESCHI-NS126-R4FA130 - radiacao_acumulada_20A	18662	3.00	391899	63.07
TREBESCHI-NS126-R4FA130 - radiacao_acumulada_20B	18701	3.01	410600	66.08
TREBESCHI-NS126-R4FA130 - radiacao_acumulada_21A	18703	3.01	429303	69.08
-TREBESCHI-NS126-R4FA130 - radiacao_acumulada_21B	18725	3.01	448028	72.10
TREBESCHI-NS200_H-R4FA125 - tensao_21A	            9631	1.55	457659	73.65
TREBESCHI-NS200_H-R4FA125 - tensao_21B	            41645	6.70	499304	80.35
TREBESCHI-NS200_H-R4FA128 - tensao_18	            9303	1.50	508607	81.85
TREBESCHI-NS200_H-R4FA128 - tensao_19A	            42267	6.80	550874	88.65
-TREBESCHI-NS200_H-R4FA128 - tensao_19B	            41895	6.74	592769	95.39
TREBESCHI-NS200_H-R4FA128 - tensao_20A	            14505	2.33	607274	97.72
TREBESCHI-NS200_H-R4FA128 - tensao_20B	            14139	2.28	621413	100.00
*/

data dpv temp umi radi ten;
set work.import2 (drop = rendered annotation);
if pointname eq 'TREBESCHI-NS126-R4FA130 - DPV'                     then output dpv;
if pointname eq 'TREBESCHI-NS126-R4FA130 - Temperatura'             then output temp;
if pointname eq 'TREBESCHI-NS126-R4FA130 - Umidade'                 then output umi;
if pointname eq 'TREBESCHI-NS126-R4FA130 - radiacao_acumulada_21B'  then output radi;
if pointname eq 'TREBESCHI-NS200_H-R4FA125 - tensao_21B'            then output ten;
run;


proc sql;
 create table max_min as
 select max(time) as time_max format = datetime16.,
        min(time) as time_min format = datetime16.
 from work.import2;
quit;

data base_modelo;
format t_ini datetime16. t_fim datetime16.;
 t_ini = '04JUN16:00:00:00'dt;
 t_fim = '04JUN16:00:00:00'dt;
 retain t_ini;
 do until(t_fim > '22SEP16:23:59:59'dt);
  %macro tamanho_interv(time);
   t_fim = intnx('minute',t_ini,&time.);
  %mend;
   %tamanho_interv(30)
  output;
  t_ini = t_fim;
 end;
run;

proc sql;
 %macro busca_infos(var);
 create table info_&var. as
 select a.*, b.value as &var., b.time
 from base_modelo as a left join &var. as b on
 a.t_ini <= b.time and a.t_fim >= b.time;
 %mend;
 %busca_infos(dpv)
 %busca_infos(temp)
 %busca_infos(umi)
 %busca_infos(radi)
 %busca_infos(ten)
quit;

proc sort data = info_dpv;
 by t_ini time;
run;

data info_dpv;
 set info_dpv;
by t_ini;
retain rep_inf rep_in rep_sup;
	if first.t_ini then do;
	 rep_inf = 0; 
	 rep_in  = 0;
	 rep_sup = 0;
	end;
	if dpv < 0.7 and not missing(dpv)         then rep_inf = rep_inf + 1; else rep_inf = 0;
	if dpv > 1.5 and not missing(dpv)         then rep_sup = rep_sup + 1; else rep_sup = 0;
	if 0.7 <= dpv <= 1.5 and not missing(dpv) then rep_in  = rep_in  + 1; else rep_in  = 0;
run;

%macro info(base,var);
proc sql;
 create table &base._f as
 select  t_ini
        ,t_fim
        ,mean(&var.)   as &var._media
        ,max(&var.)    as &var._max
        ,min(&var.)    as &var._min
        ,var(&var.)    as &var._var
        ,median(&var.) as &var._median
		%if &base. eq info_dpv %then %do;
		 ,max(rep_sup)                      as qtd_out_sup_c
		 ,max(rep_inf)                      as qtd_out_inf_c
		 ,max(rep_in)                       as qtd_in_c
		 ,count(dpv)                        as qtd_tot
		 ,sum(case when dpv < 0.7 and not missing(dpv) then 1 else 0 end) as qtd_out_inf
         ,sum(case when dpv > 1.5                      then 1 else 0 end) as qtd_out_sup
         ,sum(case when 0.7 <= dpv <= 1.5              then 1 else 0 end) as qtd_in
		%end;
 from &base.
 group by 1,2;
quit;
%mend;
%info(info_temp,temp)
%info(info_dpv,dpv)
%info(info_umi,umi)
%info(info_radi,radi)
%info(info_ten,ten)

data  base_final;
merge info_temp_f
      info_umi_f
      info_radi_f
      info_ten_f
      info_dpv_f    ;
by t_ini t_fim;
run;

data base_final;
set base_final;
where t_fim > '04JUN16:22:30:00'dt;
run; 

/*0,7 e 1,5*/

%macro gera(base);
data &base.;
set  &base. (where = (t_fim > '04JUN16:22:30:00'dt));
/*
	if      dpv_min < 0.7 then flag_dpv = -1;
	else if dpv_max > 1.5 then flag_dpv =  1;
	else                       flag_dpv =  0;
	if dpv_media = . then flag_dpv = .;
*/	
   %macro def(time,def);
    %let i = 1;
    %do %while (&i. <= &def.);
        format t_fim_def&i. datetime16.  t_ini_def&i. datetime16.;
		t_fim_def&i. = intnx('minute',t_fim,%eval(&i.*&time.));
    	t_ini_def&i. = intnx('minute',t_ini,%eval(&i.*&time.));
     %let i = %eval(&i. + 1); 	
     %end;
    %mend;
    %def(30,10);  
run;
%mend;
%gera(info_temp_f)
%gera(info_umi_f)
%gera(info_radi_f)
%gera(info_ten_f)
%gera(info_dpv_f)

data info_dpv_f_2;
set  info_dpv_f;
keep t_ini t_fim dpv_media dpv_max dpv_min dpv_var dpv_median
qtd_out_sup_c qtd_out_inf_c qtd_in_c qtd_tot qtd_out_inf
qtd_out_sup qtd_in;
run;

%macro junta(base,var);
proc sql undo_policy = none;
 create table info_dpv_f_2 as
 select k.*
        ,a.&var._media       as &var._media_t1
		,a.&var._max         as &var._max_t1  
		,a.&var._min         as &var._min_t1
		,a.&var._var         as &var._var_t1      
		,a.&var._median      as &var._median_t1
		,b.&var._media       as &var._media_t2
		,b.&var._max         as &var._max_t2  
		,b.&var._min         as &var._min_t2
		,b.&var._var         as &var._var_t2      
		,b.&var._media       as &var._median_t2
		,c.&var._media       as &var._media_t3
		,c.&var._max         as &var._max_t3  
		,c.&var._min         as &var._min_t3
		,c.&var._var         as &var._var_t3      
		,c.&var._median      as &var._median_t3
		,d.&var._media       as &var._media_t4
		,d.&var._max         as &var._max_t4  
		,d.&var._min         as &var._min_t4
		,d.&var._var         as &var._var_t4      
		,d.&var._media       as &var._median_t4
		,e.&var._media       as &var._media_t5
		,e.&var._max         as &var._max_t5  
		,e.&var._min         as &var._min_t5
		,e.&var._var         as &var._var_t5      
		,e.&var._median      as &var._median_t5
		,f.&var._media       as &var._media_t6
		,f.&var._max         as &var._max_t6  
		,f.&var._min         as &var._min_t6
		,f.&var._var         as &var._var_t6      
		,f.&var._media       as &var._median_t6
		,g.&var._media       as &var._media_t7
		,g.&var._max         as &var._max_t7  
		,g.&var._min         as &var._min_t7
		,g.&var._var         as &var._var_t7      
		,g.&var._median      as &var._median_t7
		,h.&var._media       as &var._media_t8
		,h.&var._max         as &var._max_t8  
		,h.&var._min         as &var._min_t8
		,h.&var._var         as &var._var_t8      
		,h.&var._media       as &var._median_t8
		,i.&var._media       as &var._media_t9
		,i.&var._max         as &var._max_t9  
		,i.&var._min         as &var._min_t9
		,i.&var._var         as &var._var_t9      
		,i.&var._median      as &var._median_t9
		,j.&var._media       as &var._media_t10
		,j.&var._max         as &var._max_t10  
		,j.&var._min         as &var._min_t10
		,j.&var._var         as &var._var_t10     
		,j.&var._media       as &var._median_t10
from    info_dpv_f_2      as k left join &base. as a on
        k.t_ini = a.t_ini_def1 left join &base. as b on  
        k.t_ini = b.t_ini_def2 left join &base. as c on  
        k.t_ini = c.t_ini_def3 left join &base. as d on  
        k.t_ini = d.t_ini_def4 left join &base. as e on  
        k.t_ini = e.t_ini_def5 left join &base. as f on  
        k.t_ini = f.t_ini_def6 left join &base. as g on  
        k.t_ini = g.t_ini_def7 left join &base. as h on
        k.t_ini = h.t_ini_def8 left join &base. as i on
        k.t_ini = i.t_ini_def9 left join &base. as j on
        k.t_ini = j.t_ini_def10;
quit;
%mend;
%junta(info_temp_f,temp)
%junta(info_dpv_f,dpv)
%junta(info_umi_f,umi)
%junta(info_radi_f,radi)
%junta(info_ten_f,ten)
  


proc contents data = info_dpv_f_2; run;

data info_dpv_f_3
(drop = dpv_mediana inf sup 'in'n 
qtd_out_sup_c qtd_out_inf_c qtd_in_c qtd_tot qtd_out_inf
qtd_out_sup qtd_in);
set info_dpv_f_2; 
	if      dpv_min < 0.7 then flag_dpv_min_max = -1;
	else if dpv_max > 1.5 then flag_dpv_min_max =  1;
	else                       flag_dpv_min_max =  0;
	if dpv_media = . then flag_dpv_min_max = .;
	
	if      dpv_media < 0.7 then flag_dpv_media = -1;
	else if dpv_media > 1.5 then flag_dpv_media =  1;
	else                       flag_dpv_media =  0;
	if dpv_media = . then flag_dpv_media = .;

	if      dpv_median < 0.7 then flag_dpv_median = -1;
	else if dpv_median > 1.5 then flag_dpv_median =  1;
	else                       flag_dpv_median =  0;
	if dpv_median = . then flag_dpv_median = .;

    inf = sum(qtd_out_inf, qtd_out_inf_c) / qtd_tot;
    sup = sum(qtd_out_sup, qtd_out_sup_c) / qtd_tot; 
    in  = sum(qtd_in     , qtd_in_c     ) / qtd_tot;

	if   max(inf,sup,in) = in  then flag_dpv_2 = 0;        
    if   max(inf,sup,in) = inf then flag_dpv_2 = -1;
    if   max(inf,sup,in) = sup then flag_dpv_2 = 1;    
    
    inf = sum(qtd_out_inf) / qtd_tot;
    sup = sum(qtd_out_sup) / qtd_tot; 
    in  = sum(qtd_in)      / qtd_tot;    
    
	if   max(inf,sup,in) = in  then flag_dpv_3 = 0;        
    if   max(inf,sup,in) = inf then flag_dpv_3 = -1;
    if   max(inf,sup,in) = sup then flag_dpv_3 = 1;    
   
run;
